from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import ListView
import json

from order_management.models import Article, Order, OrderedArticleQuantity
from order_management.signals import order_created


class ArticlesListView(LoginRequiredMixin, ListView):
    model = Article

    def post(self, request):
        data = request.POST
        article_quantity = json.loads(data.get('articleQuantity'))
        pre_checkout_data = {'items': []}
        total_price = 0
        for article_id, quantity in article_quantity.items():
            article = Article.objects.get(id=article_id)
            pre_checkout_data['items'].append(
                dict(
                    reference=article.reference,
                    name=article.name,
                    price=article.price,
                    quantity=quantity)
            )
            total_price += article.price * quantity
        pre_checkout_data['total_price'] = total_price

        return render(request,
                      template_name='order_management/pre_checkout.html',
                      context={'pre_checkout_data': pre_checkout_data})


@login_required
def do_checkout(request):
    data = request.POST
    pre_checkout_data = json.loads(data.get('preCheckoutData'))
    user = request.user
    order = Order.objects.create(user=user)
    for item in pre_checkout_data['items']:
        article = Article.objects.get(reference=item['reference'])
        ordered_quantity = OrderedArticleQuantity.objects.create(
            order=order,
            article=article,
            quantity=item['quantity'])
    messages.success(request, 'Order #{} has been created successfully'.format(order.id))
    # TODO: Do it with Signals
    order_created.send(sender=do_checkout, order=order)
    return redirect('articles')
