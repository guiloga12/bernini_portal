from django.core.mail import EmailMultiAlternatives
from django.dispatch import Signal, receiver
from settings import EMAIL_HOST_USER as email_address

order_created = Signal()

# TODO: Move email templates to html, txt files.
HTML_TEMPLATE = """
<h1>Order notification</h1>
<p>Hello this is a fake notification template</p>
%s
"""
TEXT_CONTENT = 'Order notification\n Hello this is a fake notification %s'


@receiver(order_created)
def notify_order(order, **kwargs):
    # TODO: Move this into another module for sending emails.
    # TODO: Create a csv/xml file containing ordered_articles and attach it to the message.
    subject = f'Order #{order.id}'
    ordered_articles = order.ordered_articles.all()
    msg = EmailMultiAlternatives(
        subject, TEXT_CONTENT % str(ordered_articles), email_address, [email_address,])
    msg.attach_alternative(HTML_TEMPLATE % str(ordered_articles), "text/html")
    msg.send()
