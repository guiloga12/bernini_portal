from django.db import models
from django.contrib.auth import get_user_model

from order_management.apps import OrderManagementConfig

UserORM = get_user_model()


class Article(models.Model):
    reference = models.CharField(max_length=30, unique=True)
    name = models.CharField(max_length=30)
    description = models.TextField()
    image = models.ImageField(upload_to='articles/')
    price = models.FloatField()

    class Meta:
        db_table = f"{OrderManagementConfig.name}_articles"

    def __str__(self):
        return "#{0} {1}".format(self.reference, self.name)


class Order(models.Model):
    user = models.ForeignKey(UserORM, on_delete=models.CASCADE, related_name='orders')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(
        max_length=30,
        choices=[('pending', 'Pending'),
                 ('scheduled', 'Scheduled'),
                 ('dispatched', 'Dispatched')],
        default='pending')

    @property
    def total(self):
        articles = self.articles.all()
        total = sum([item.price for item in articles]) if len(articles) != 0 else 0
        return total

    class Meta:
        db_table = f"{OrderManagementConfig.name}_orders"
        get_latest_by = ['created', 'updated']

    def __str__(self):
        return "#{0} [{1}] odered by {2}".format(
            self.id, self.created, self.user.username)


class OrderedArticleQuantity(models.Model):
    quantity = models.IntegerField()
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='ordered_quantities')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='ordered_articles')

    class Meta:
        db_table = f"{OrderManagementConfig.name}_ordered_article_quantities"
        verbose_name_plural = 'Ordered article quantities'

    def __str__(self):
        return "#{0} order: {1} - Ordered {2} units of {3}".format(
            self.id, self.order.id, self.quantity, self.article.name)
