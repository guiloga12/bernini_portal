from django.urls import path
from order_management.views import ArticlesListView, do_checkout


urlpatterns = [
    path('articles/', ArticlesListView.as_view(), name='articles'),
    path('checkout/', do_checkout, name='do_checkout'),
]
