from django.contrib import admin
from .models import Article, Order, OrderedArticleQuantity


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderedArticleQuantity)
class OrderedArticleQuantityAdmin(admin.ModelAdmin):
    pass
