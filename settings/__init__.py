import os

from .common import *

ENVIRONMENT = os.getenv('ENVIRONMENT', 'development')

if ENVIRONMENT == 'production':
    from .prod import *
else:
    from .dev import *
