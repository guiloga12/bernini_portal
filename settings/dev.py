from pathlib import Path
from .common import BASE_DIR

DEBUG = True

ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
]

FIXTURE_DIRS = [
    Path(BASE_DIR, 'fixtures/'),
]

MEDIA_FIXTURES_FILES_DIRS = [
    Path(BASE_DIR, 'fixtures/img'),
]

# Email Backends
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST_USER = 'wyl.loga@gmail.com'
