"""bernini_portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import RedirectView
from django.urls import path, include, re_path
from rest_framework.schemas import get_schema_view


from api.urls import urlpatterns as api_url_patterns

import settings

urlpatterns = [
    re_path(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(pattern_name='login', permanent=False)),
    path('login/', auth_views.LoginView.as_view(template_name='login.html',
                                                redirect_authenticated_user=True),
         name='login'),
    path('logout/', auth_views.LogoutView.as_view(),
         name='logout'),

    path('order-management/', include('order_management.urls')),

    # Api urls
    *api_url_patterns,

    # OpenAPI
    path('openapi', get_schema_view(title="Bernini Portal",
                                    description="Order Management API",
                                    version="1.0.0",
                                    patterns=api_url_patterns),
         name='openapi-schema'),
]

# serve media files in Development
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
