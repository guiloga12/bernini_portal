#!/bin/sh

# migrate database
python manage.py makemigrations
python manage.py migrate

# create superuser
echo "from django.contrib.auth import \
      get_user_model; User = get_user_model(); \
      User.objects.create_superuser('admin', 'admin@berniniportal.com', 'admin')" | python manage.py shell

echo "A development User has been created with:"
echo "username: admin"
echo "password: admin"

mkdir media
chmod -R 777 media

cp -R fixtures/img media/articles
python manage.py loaddata articles.json
