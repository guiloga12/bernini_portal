from django.urls import path, include
from rest_framework import routers

from .views import ArticleViewSet

api_router = routers.DefaultRouter()
api_router.register(r'articles', ArticleViewSet)


urlpatterns = [
    path('api/', include(api_router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
